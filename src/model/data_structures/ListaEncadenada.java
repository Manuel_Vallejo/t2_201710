package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> primero;
	private NodoSencillo<T> ultimo;
	private NodoSencillo<T> referencia;
	private int cantidad;
	private int posicionReferencia;

	public ListaEncadenada() {
		primero = null;
		ultimo = null;
		referencia = null;
		cantidad = 0;
		posicionReferencia = 0;
	}
	

	@Override
	public Iterator<T> iterator() {
		posicionReferencia = 0;
		return new Iterator<T>() {

			@Override
			public boolean hasNext() {
				return (posicionReferencia < cantidad);
			}

			@Override
			public T next() {				
				if(hasNext()){
					return (T) darElemento(posicionReferencia++);}
					else return null;
			
		}};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(primero == null){
			ultimo = new NodoSencillo<T>(elem, null);
			primero = ultimo;
			referencia = primero;
			cantidad++;
		}
		else{
			NodoSencillo<T> nuevo = new NodoSencillo<T>(elem, null);
			ultimo.setSiguiente(nuevo);
			ultimo = nuevo;			
			cantidad++;
		}

	}

	@Override
	public T darElemento(int pos) {
		boolean res = false;
		T dat = null;
		if(primero !=null){
			int x = 0;
			NodoSencillo<T> actual = primero;
			while(!res){
				if(x == pos){
					dat = actual.getDato();
					res =true;
				}
				else{
					actual = actual.getSiguiente();
					x++;
				}
			}
		}
		return dat;
	}


	@Override
	public int darNumeroElementos() {
		return cantidad;
	}

	@Override
	public T darElementoPosicionActual() {
		return referencia.getDato();
	}

	@Override
	public boolean avanzarSiguientePosicion()  {
		if(referencia.getSiguiente()==null){
			return false;
		}
		referencia=referencia.getSiguiente();
		return true;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		boolean x = false;
		if(posicionReferencia-1 > 0){
			posicionReferencia--;
			x = true;
		}
		return x;
	}

}
