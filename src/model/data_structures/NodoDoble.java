package model.data_structures;

public class NodoDoble<T> {
	
	private T dato;
	private NodoDoble<T> siguiente;
	private NodoDoble<T> anterior;

	public NodoDoble( T pDato, NodoDoble<T> ant, NodoDoble<T> sig){
		anterior = ant;
		siguiente = sig;
		dato = pDato;
	}

	public T getDato() {
		return dato;
	}

	public void setDato(T pDato) {
		this.dato = pDato;
	}

	public NodoDoble<T> getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(NodoDoble<T> pSiguiente) {
		this.siguiente = pSiguiente;
	}

	public NodoDoble<T> getAnterior() {
		return anterior;
	}

	public void setAnterior(NodoDoble<T> pAnterior) {
		this.anterior = pAnterior;
	}
	
		
}
 
