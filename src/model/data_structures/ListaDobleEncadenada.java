package model.data_structures;
import java.util.Iterator;
import java.util.ListIterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> inicio;
	private NodoDoble<T> fin;
	private int cantidad;
	private int posicionReferencia;
	private NodoDoble<T> nodoReferencia;

	public ListaDobleEncadenada(){
		inicio = null;
		fin = null;
		cantidad = 0;
		posicionReferencia = -1;
		nodoReferencia = inicio;
	}

	@Override
	public Iterator<T> iterator(){
		return new ListIterator<T>() {
			public void add(T arg0) {
				agregarElementoFinal(arg0);				
			}
			public boolean hasNext() {
				return (posicionReferencia <= cantidad-1);
			}

			@Override
			public boolean hasPrevious() {
				return (posicionReferencia > 0);
			}

			@Override
			public T next() {
				return darElemento(posicionReferencia++);
			}

			@Override
			public int nextIndex() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public T previous() {
				return darElemento(posicionReferencia--);
			}

			@Override
			public int previousIndex() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void set(T arg0) {
				// TODO Auto-generated method stub
				
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if (inicio == null){
			fin = new NodoDoble<T>(elem, null, null);
			inicio = fin;
			nodoReferencia = inicio;
			posicionReferencia++;
			cantidad++;
		}
		else{
			NodoDoble<T> nuevo = new NodoDoble<T>(elem, fin, null);
			fin.setSiguiente(nuevo);
			fin = nuevo;
			cantidad++;
		}
	}

	@Override
	public T darElemento(int pos) {
		T dat = null;
		if (inicio != null){
			boolean res = false;
			int x = 0;
			NodoDoble<T> act = inicio;
			while(res==false){
				if(x == pos){
					res = true;
					dat = act.getDato();
				}
				else{
					act = act.getSiguiente();
					x++;
				}
			}
		}
		return dat;	

	}


	@Override
	public int darNumeroElementos() {
		return cantidad;
	}

	@Override
	public T darElementoPosicionActual() {
		return nodoReferencia.getDato();	
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if(nodoReferencia!=null && nodoReferencia.getSiguiente()!=null){
			nodoReferencia = nodoReferencia.getSiguiente();
			return true;
		}
		else{ 
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		if(nodoReferencia!=null && nodoReferencia.getAnterior()!=null ){
			nodoReferencia = nodoReferencia.getAnterior();
			return true;
		}
		else {
			return false;
		}

	}

}
