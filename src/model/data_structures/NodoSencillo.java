package model.data_structures;

public class NodoSencillo<T> {

	private T dato;
	private NodoSencillo<T> siguiente;

	public NodoSencillo( T pDato,NodoSencillo<T> pSiguiente){
		siguiente = pSiguiente;
		dato = pDato;
	}
	
	public T getDato(){
        return dato;
    }
	
	public NodoSencillo<T> getSiguiente(){
		return siguiente;
	}
	
	public void setDato(T pDato){
        dato = pDato;
    }

    public void setSiguiente(NodoSencillo<T> pSiguiente) {
        siguiente = pSiguiente;
    }

}
