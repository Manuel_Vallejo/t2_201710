package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;	

	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub 
		try {
			BufferedReader br = new BufferedReader(new FileReader(archivoPeliculas));
			String line = br.readLine();
			misPeliculas = new ListaEncadenada<>();
			line = br.readLine();
			while(null != line){
				String [] x = line.split(",");
				if(x.length > 3){
					String apoyo = "";
					for(int i = 1;i<x.length -1;i++){
						apoyo += x[i];
					}
					x[1] = apoyo;
					x[2] = x[3];
					x[3] = null;
				}	
				VOPelicula nueva = new VOPelicula();
				String tit;
				Integer agn = 0;
				try{
					tit = x[1].substring(0, x[1].indexOf("("));
					agn = Integer.parseInt(x[1].substring(x[1].indexOf("(")+1, x[1].indexOf(")")));
				}catch(Exception e){
					tit = x[1];						
				}
				nueva.setTitulo(tit);
				String generoos = x[2];					
				nueva.setAgnoPublicacion(agn);					
				ListaEncadenada<String> listGen = new ListaEncadenada<>();
				String [] gen = generoos.split("[|]");
				for(int i = 0;i<gen.length;i++){
					listGen.agregarElementoFinal(gen[i]);
				}
				nueva.setGenerosAsociados(listGen);
				misPeliculas.agregarElementoFinal(nueva);
				line = br.readLine();
			}
			peliculasAgno = new ListaDobleEncadenada<>();
			int ag = 1950;
			while(ag<=2016){
				ListaEncadenada<VOPelicula> sub = new ListaEncadenada<>();
				for (int i = 0;i<misPeliculas.darNumeroElementos();i++){				
					VOPelicula actual = misPeliculas.darElemento(i);
					if(actual.getAgnoPublicacion() == ag){
						sub.agregarElementoFinal(actual);
					}
				}
				VOAgnoPelicula temp = new VOAgnoPelicula();
				temp.setAgno(ag);
				temp.setPeliculas(sub);
				peliculasAgno.agregarElementoFinal(temp);
				temp=null;
				ag++;
			}
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();			
		}

	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		ListaEncadenada<VOPelicula> rpta = new ListaEncadenada<>();
		for(int i = 0; i<misPeliculas.darNumeroElementos();i++){
			VOPelicula actual = misPeliculas.darElemento(i);
			if (actual.getTitulo().contains(busqueda)){
				rpta.agregarElementoFinal(actual);
			}
		}
		return rpta;
	}


	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		ListaEncadenada<VOPelicula> rpta = new ListaEncadenada<>();

		for(int i = 0; i < peliculasAgno.darNumeroElementos();i++){

			VOAgnoPelicula actual = peliculasAgno.darElemento(i);

			if(actual.getAgno() == agno && agno >= 1950){

				rpta = (ListaEncadenada<VOPelicula>) actual.getPeliculas();

				break;
			}
		}

		return rpta;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {

		if(peliculasAgno==null)
			return null;
		if (peliculasAgno.avanzarSiguientePosicion())
			return peliculasAgno.darElementoPosicionActual();
		else
			return null;

	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		if(peliculasAgno==null)
			return null;
		return (peliculasAgno.retrocederPosicionAnterior()) ? peliculasAgno.darElementoPosicionActual():null;
	}

}
