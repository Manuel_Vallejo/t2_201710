package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest extends TestCase{

	private ListaEncadenada<String> lista;

	public void setupEscenario1(){
		lista = new ListaEncadenada<String>();
		lista.agregarElementoFinal("a");
		lista.agregarElementoFinal("b");
		lista.agregarElementoFinal("c");
	}

	public void testAgregarElementoFinal(){
		setupEscenario1();
		assertEquals("No fue lo esperado.", "a",lista.darElemento(0));
		assertEquals("No fue lo esperado.", "b",lista.darElemento(1));
		assertEquals("No fue lo esperado.", "c",lista.darElemento(2));
	}

	public void testNumeroDeElementos(){
		setupEscenario1();
		assertNotNull( "Deber�a retornar una lista.", lista);
		assertEquals("La lista deber�a tener 3.", 3, lista.darNumeroElementos());
	}

	public void testDarElementosPosicion(){
		setupEscenario1();
		assertEquals("la", "a", lista.darElementoPosicionActual());
		assertTrue(lista.avanzarSiguientePosicion());
		assertEquals("la", "b", lista.darElementoPosicionActual());
		assertTrue(lista.avanzarSiguientePosicion());
		assertEquals("la", "c", lista.darElementoPosicionActual());
		assertFalse(lista.avanzarSiguientePosicion());
	}
}
