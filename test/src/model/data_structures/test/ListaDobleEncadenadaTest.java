package model.data_structures.test;
import model.data_structures.ListaDobleEncadenada;
import junit.framework.TestCase;

public class ListaDobleEncadenadaTest extends TestCase{
	private ListaDobleEncadenada<String> lista;
	
	public void setUpEscenario1(){
		lista = new ListaDobleEncadenada<>();
	}
	
	public void setUpEscenario2(){
		setUpEscenario1();
		lista.agregarElementoFinal("a");
		lista.agregarElementoFinal("b");
		lista.agregarElementoFinal("c");
		lista.agregarElementoFinal("d");
	}
	
	/**
	 * Prueba 1: Agregar elementos a la lista.
	 * Metodos a probar:
	 * agregarElementoFinal
	 * avanzarsiguienteposicion
	 * darElemento
	 */
	public void testAgregarElementoFinal(){
		setUpEscenario1();
		lista.agregarElementoFinal("a");
		lista.agregarElementoFinal("b");
		lista.agregarElementoFinal("c");
		lista.agregarElementoFinal("d");
		assertEquals("No es el esperado", "a", lista.darElemento(0));
		assertEquals("No es el esperado", "b", lista.darElemento(1));
		assertEquals("No es el esperado", "c", lista.darElemento(2));
		assertEquals("No es el esperado", "d", lista.darElemento(3));		
	}
	
	/**
	 * Prueba 2: Dar numero de elementos.
	 * Metodos a probar:
	 * darNumeroElementos
	 */
	public void testdarNumeroElementos(){
		setUpEscenario1();		
		assertEquals("No era el esperado", 0, lista.darNumeroElementos());
		setUpEscenario2();
		assertEquals("No era el esperado", 4, lista.darNumeroElementos());
	}
	
	
	/**
	 * Prueba 3: Dar elemento en la posicion actual.
	 * Metodos a probar:
	 * avanzarSiguientePosicion
	 * retrocederPosiconAnterior
	 * darElementoPOsicionActual
	 */
	public void testDarElementoPosicionActual(){
		setUpEscenario2();
		assertFalse(lista.retrocederPosicionAnterior());
		assertEquals("a", lista.darElementoPosicionActual());
		assertTrue(lista.avanzarSiguientePosicion());
		assertEquals("b", lista.darElementoPosicionActual());
		assertTrue(lista.avanzarSiguientePosicion());
		assertEquals("c", lista.darElementoPosicionActual());
		assertTrue(lista.avanzarSiguientePosicion());
		assertEquals("d", lista.darElementoPosicionActual());
		assertFalse(lista.avanzarSiguientePosicion());
		assertTrue(lista.retrocederPosicionAnterior());
		assertEquals("c", lista.darElementoPosicionActual());
		
		
	}
	


}
